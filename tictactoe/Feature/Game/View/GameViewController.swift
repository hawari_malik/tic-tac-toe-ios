//
//  GameViewController.swift
//  tictactoe
//
//  Created by IT Resource Center on 19/07/19.
//  Copyright © 2019 hawari_am. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa

class GameViewController: UIViewController {
    
    private let GRID_SIZE = 3
    
    @IBOutlet var buttons: [UIButton]!
    @IBOutlet weak var playerLabel: UILabel!
    @IBOutlet weak var botLabel: UILabel!
    
    private let disposeBag = DisposeBag()
    private let viewModel: GameViewModel
    
    init() {
        self.viewModel = GameViewModel(gridSize: self.GRID_SIZE, disposeBag: self.disposeBag)
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(code:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Tic Tac Toe"
        
        self.setupGame()
    }
    
    private func setupGame(){
        
        self.viewModel.redraw.asDriver(onErrorJustReturn: Void())
            .drive(onNext:{
                print("drawing")
                let cellState = self.viewModel.getCellState()
                // Set cell color
                for i in 0 ..< cellState.count {
                    self.buttons[i].backgroundColor = cellState[i].value
                }
                
                // Set text
                switch self.viewModel.getGameState(){
                case .start:
                    self.playerLabel.text = "Player Turn"
                    self.botLabel.text = ""
                case .playerTurn:
                    self.playerLabel.text = ""
                    self.botLabel.text = "Bot Turn"
                case .botTurn:
                    self.playerLabel.text = "Player Turn"
                    self.botLabel.text = ""
                case .playerWin:
                    self.playerLabel.text = "WIN"
                    self.playerLabel.textColor = UIColor.blue
                    self.botLabel.text = ""
                case .botWin:
                    self.playerLabel.text = ""
                    self.botLabel.text = "WIN"
                    self.botLabel.textColor = UIColor.red
                case .draw:
                    self.playerLabel.text = "DRAW"
                    self.playerLabel.textColor = UIColor.green
                    self.botLabel.text = "DRAW"
                    self.botLabel.textColor = UIColor.green
                }
                
                // Tells viewModel to advance to next turn
                // helps prevents data race on the observables
                // and also conviniently adds delay on the bot
                self.perform(#selector(self.advTurn), with: nil, afterDelay: 0.5)
        }).disposed(by: disposeBag)
        
    }
    
    @objc private func advTurn(){
        print("next turn order")
        self.viewModel.advanceTurn.onNext(Void())
    }
    
    @IBAction func buttonOnClick(_ sender: UIButton) {
        self.viewModel.buttonListener.onNext(sender.tag)
    }
    

}
