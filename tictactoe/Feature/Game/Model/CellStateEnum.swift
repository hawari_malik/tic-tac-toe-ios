//
//  GameEnums.swift
//  tictactoe
//
//  Created by IT Resource Center on 19/07/19.
//  Copyright © 2019 hawari_am. All rights reserved.
//

import Foundation
import UIKit

enum CellState{
    case empty
    case bot
    case player
}

extension CellState{
    var value: UIColor {
        get {
            switch self {
                case .empty:
                    return #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
                case .bot:
                    return #colorLiteral(red: 0.9686274529, green: 0.78039217, blue: 0.3450980484, alpha: 1)
                case .player:
                    return #colorLiteral(red: 0.3411764801, green: 0.6235294342, blue: 0.1686274558, alpha: 1)
            }
        }
    }
}
