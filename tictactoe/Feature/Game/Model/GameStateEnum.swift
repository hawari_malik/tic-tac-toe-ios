//
//  GameStateEnum.swift
//  tictactoe
//
//  Created by IT Resource Center on 22/07/19.
//  Copyright © 2019 hawari_am. All rights reserved.
//

import Foundation

enum GameState{
    case start
    case playerTurn
    case botTurn
    case playerWin
    case botWin
    case draw
}
