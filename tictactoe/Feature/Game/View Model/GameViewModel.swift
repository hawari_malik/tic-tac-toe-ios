//
//  GameViewModel.swift
//  tictactoe
//
//  Created by IT Resource Center on 22/07/19.
//  Copyright © 2019 hawari_am. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa

class GameViewModel {
    
    private let gridSize: Int
    private let gameState: BehaviorRelay<GameState>
    private let cellState: BehaviorRelay<[CellState]>
    private let disposeBag: DisposeBag
    
    public let advanceTurn = PublishSubject<Void>()
    public let buttonListener = PublishSubject<Int>()
    public let redraw = BehaviorSubject<Void>(value: Void())
    
    init(gridSize:Int, disposeBag: DisposeBag){
        self.gridSize = gridSize
        self.gameState = BehaviorRelay.init(value: .start)
        
        let cellCount = self.gridSize * self.gridSize
        var cellArray:[CellState] = []
        for _ in 1...cellCount{
            cellArray.append(.empty)
        }
        
        self.cellState = BehaviorRelay.init(value: cellArray)
        self.disposeBag = disposeBag
        
        self.setupGame()
    }
    
    private func setupGame() {
        
        // Player input logic
        self.buttonListener
            .filter { _ in self.gameState.value == .playerTurn }
            .filter { self.cellState.value[$0] == .empty }
            .subscribe(onNext:{
                print("player running")
                var stateArray = self.cellState.value
                stateArray[$0] = .player
                self.cellState.accept(stateArray)
            }).disposed(by: self.disposeBag)
        
        // Bot turn logic
        self.gameState
            .filter { $0 == .botTurn }
            .subscribe(onNext:{ _ in
                print("bot running")
                var moves:[Int] = []
                var stateArray = self.cellState.value
                
                // Find possible moves
                stateArray.enumerated().forEach{ (index, state) in
                    if(state == .empty){
                        moves.append(index)
                    }
                }
                
                let index = moves[Int.random(in: 0 ..< moves.count)]
                stateArray[index] = .bot
                self.cellState.accept(stateArray)
            }).disposed(by: self.disposeBag)
        
        // Gameboard check logic
        self.cellState
            .subscribe(onNext:{ stateArray in
                print("checking board")
                // Convert to 2D Array for easier check
                let stateArray2D = Helper.convertArrayTo2D(stateArray, size: self.gridSize)
                var winFlag = CellState.empty
                
                // Horizontal check logic
                for row in stateArray2D{
                    var colorCheck = row[0]
                    for cell in row {
                        if(cell != colorCheck){
                            colorCheck = .empty
                            break
                        }
                    }
                    if (colorCheck != .empty){
                        winFlag = colorCheck
                        break
                    }
                }
                
                // Vertical check logic
                if(winFlag == .empty){
                    for i in 0 ..< stateArray2D.count{
                        var colorCheck = stateArray2D[0][i]
                        for j in 1 ..< stateArray2D.count {
                            let cell = stateArray2D[j][i]
                            if(cell != colorCheck){
                                colorCheck = .empty
                                break
                            }
                        }
                        if (colorCheck != .empty){
                            winFlag = colorCheck
                            break
                        }
                    }
                }
                
                // \ Diagonal check logic
                if(winFlag == .empty){
                    var colorCheck = stateArray2D[0][0]
                    for i in 0 ..< stateArray2D.count{
                        let cell = stateArray2D[i][i]
                        if(cell != colorCheck){
                            colorCheck = .empty
                            break
                        }
                        
                    }
                    if (colorCheck != .empty){
                        winFlag = colorCheck
                    }
                }
                
                // / Diagonal check logic
                if(winFlag == .empty){
                    var colorCheck = stateArray2D[self.gridSize - 1][0]
                    for i in 0 ..< stateArray2D.count{
                        let cell = stateArray2D[self.gridSize - (1 + i)][i]
                        if(cell != colorCheck){
                            colorCheck = .empty
                            break
                        }
                        
                    }
                    if (colorCheck != .empty){
                        winFlag = colorCheck
                    }
                }
                
                // Draw check logic
                var drawFlag = true
                if(winFlag == .empty){
                    for state in stateArray{
                        if(state == .empty){
                            drawFlag = false
                            break
                        }
                    }
                }
                
                // Check game stopping conditions ( Player/Bot Win, Draw )
                if(winFlag != .empty){
                    let winState: GameState = winFlag == .player ? .playerWin : .botWin
                    self.gameState.accept(winState)
                } else if (drawFlag){
                    self.gameState.accept(.draw)
                }
                
                // Redraw
                print("redraw order")
                self.redraw.onNext(Void())
                
            }).disposed(by: self.disposeBag)
        
        // Next turn switches
        self.advanceTurn
            .subscribe(onNext:{
                print("next turn order received")
                switch self.gameState.value{
                case .start:
                    print("starting")
                    switch Int.random(in: 0...1){
                    case 1:
                        self.gameState.accept(.playerTurn)
                    default:
                        self.gameState.accept(.botTurn)
                    }
                case .playerTurn:
                    print("switching to bot's turn")
                    self.gameState.accept(.botTurn)
                case .botTurn:
                    print("switching to players's turn")
                    self.gameState.accept(.playerTurn)
                default:
                    return
                }
            }).disposed(by: self.disposeBag)
    }
    
    public func getGameState() -> GameState {
        return self.gameState.value
    }
    
    public func getCellState() -> [CellState] {
        return self.cellState.value
    }
}
