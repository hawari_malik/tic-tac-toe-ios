//
//  ViewController.swift
//  tictactoe
//
//  Created by IT Resource Center on 19/07/19.
//  Copyright © 2019 hawari_am. All rights reserved.
//

import UIKit

class HomeViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Home"
        // Do any additional setup after loading the view.
    }
    @IBAction func startClick(_ sender: UIButton) {
        self.navigationController?.pushViewController(GameViewController(), animated: true)
    }

}

