//
//  Helper.swift
//  tictactoe
//
//  Created by IT Resource Center on 22/07/19.
//  Copyright © 2019 hawari_am. All rights reserved.
//

import Foundation

class Helper {
    static func convertArrayTo2D<T>(_ array:[T], size:Int) -> [[T]] {
        var array2DOpt: [[T?]] = Array(repeating: Array(repeating: nil, count: size), count: size)
        for (index, value) in array.enumerated() {
            let row = index / size
            let column = index % size
            array2DOpt[row][column] = value
        }
        var array2D: [[T]] = []
        for row in array2DOpt {
            var array1D: [T] = []
            for column in row {
                array1D.append(column!)
            }
            array2D.append(array1D)
        }
        return array2D
    }
}
